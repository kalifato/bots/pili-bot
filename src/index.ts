import dotenv from 'dotenv';
import TelegramBot, { Message } from 'node-telegram-bot-api';

const result = dotenv.config();

if (result.error) {
  throw result.error;
}

const token = process.env.TELEGRAM_TOKEN || 'not-defined';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, { polling: true });

bot.on('pinned_message', (msg: Message) => {
  const chatId = msg.chat.id;

  let resp = '';

  resp = 'Mensaje pinned';

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});

// Matches "/echo [whatever]"
bot.onText(/\/echo (.+)/, (msg: Message, match: RegExpExecArray | null) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message

  console.log(`Mensaje echo ${msg.text}`);
  const chatId = msg.chat.id;

  let resp = '';
  if (match !== null) {
    [resp] = match;
  }

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', (msg: Message) => {
  const chatId = msg.chat.id;

  if (msg.reply_to_message) {
    bot.sendMessage(msg.chat.id, 'Storing message');
  } else {
    bot.sendMessage(chatId, `Received your message: ${msg.text}`);
  }
});
